package isogram

import (
	"unicode"
)

// IsIsogram checks whether the input string is an isogram.
func IsIsogram(str string) bool {
	mem := make(map[rune]int)
	for _, originalR := range str {
		r := unicode.ToLower(originalR)
		if !unicode.IsLetter(r) {
			continue
		}

		mem[r] = mem[r] + 1
		if mem[r] > 1 {
			return false
		}
	}

	return true
}
